## Productivity Timer

**Features:**

- Set timer in minutes.
- Rate productivity on a scale of 0 to 10.
- Display last session score and average score.
- Change theme between Light, Dark, and Dracula modes.
- Audible alert upon timer completion.
- Random background color change upon timer completion.

---

### Productivity Timer Versions:

1. **Web Version:**
Built with HTML, CSS, and JavaScript. It allows users to set a timer, rate their productivity, and switch between Light, Dark, and Dracula themes.

[OpenPomodoroPY Web Version](https://openpomodoropy-krafi-be669e14bbacb2a226f9fbe5685999b635de1aaaa4.gitlab.io)
https://openpomodoropy-krafi-be669e14bbacb2a226f9fbe5685999b635de1aaaa4.gitlab.io

2. **Console Version (Python):**
   - A console-based version written in Python. Users can run it in their terminal or command prompt.

3. **GUI Version (Tkinter):**
   - A graphical user interface (GUI) version also written in Python using Tkinter. It provides a visual timer and rating interface.
