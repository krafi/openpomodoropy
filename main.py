import time
import platform
from tqdm import tqdm
import os

def buzzer():
    duration = 1  # seconds
    freq = 440  # Hz
    if platform.system() == "Windows":
        import winsound
        winsound.Beep(freq, int(duration * 1000))
    else:
        # Use 'play' command which is part of 'sox' package
        os.system(f'play -nq -t alsa synth {duration} sine {freq}')

def main():
    results = []
    
    while True:
        try:
            minutes = input("Set timer (in minutes): ")
            minutes = float(minutes)
            seconds = int(minutes * 60)
            print(f"Timer set for {minutes:.2f} minutes. Starting now...")
            
            with tqdm(total=seconds, desc="Countdown", unit="s", ncols=100) as pbar:
                update_interval = 10  # Update interval in seconds
                for remaining in range(seconds, 0, -update_interval):
                    update_seconds = min(update_interval, remaining)
                    pbar.update(update_seconds)
                    time.sleep(update_seconds)
            
            buzzer()
            print("Time's up!")  
            
            score = input("Rate your productivity out of 10 (or type 'exit' to finish): ")
            if score.lower() == 'exit':
                break
            
            try:
                score_float = float(score)
                if 0 <= score_float <= 10:
                    results.append(score_float)
                else:
                    print("Please enter a score between 0 and 10.")
            except ValueError:
                print("Invalid input. Please enter a numerical value between 0 and 10.")
            
            continue_choice = input("Do you want to continue? Press Enter to continue or type 'exit' to finish: ")
            if continue_choice.lower() == 'exit':
                break

        except ValueError:
            print("Invalid time input. Please enter a numerical value for minutes.")
    
    if results:
        print("\nSession Summary:")
        for i, result in enumerate(results, 1):
            print(f"Session {i}: {result}/10")
        
        average = sum(results) / len(results)
        print(f"\nAverage Productivity Score: {average:.2f}/10")
    else:
        print("No productivity scores recorded.")

if __name__ == "__main__":
    main()
