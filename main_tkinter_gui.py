import platform
import os
import tkinter as tk
from tkinter import simpledialog, messagebox
from tkinter.ttk import Progressbar

total_elapsed_time = 0  # Global variable to track total elapsed time

def buzzer():
    duration = 1  # seconds
    freq = 440  # Hz
    if platform.system() == "Windows":
        import winsound
        winsound.Beep(freq, int(duration * 1000))
    else:
        # Use 'play' command which is part of 'sox' package
        os.system(f'play -nq -t alsa synth {duration} sine {freq}')

def start_timer(minutes, timer_label, score_label, avg_score_label, total_elapsed_label, results, score_entry, submit_button):
    global total_elapsed_time
    seconds = int(minutes * 60)
    total_elapsed_time += seconds  # Increment total elapsed time
    update_interval = 1000  # Update interval in milliseconds

    def update_remaining_time():
        nonlocal seconds
        timer_label.config(text=f"Time Remaining: {seconds // 60:02}:{seconds % 60:02}")
        seconds -= 1
        if seconds >= 0:
            timer_label.after(update_interval, update_remaining_time)
        else:
            buzzer()
            score_label.pack()  # Show the score label
            score_entry.pack()  # Show the score entry widget
            submit_button.pack()  # Show the submit button
            total_elapsed_label.config(text=f"Total Elapsed Time: {total_elapsed_time // 60} minutes {total_elapsed_time % 60} seconds")

    update_remaining_time()

def submit_score(score_entry, root, results, score_label, avg_score_label, submit_button):
    score_str = score_entry.get()
    try:
        score = float(score_str)
        if 0 <= score <= 10:
            results.append(score)
            update_labels(score_label, avg_score_label, results)
            score_entry.delete(0, tk.END)  # Clear the entry
            score_entry.pack_forget()  # Hide the score entry widget
            submit_button.pack_forget()  # Hide the submit button
        else:
            messagebox.showerror("Error", "Please enter a score between 0 and 10.")
    except ValueError:
        messagebox.showerror("Error", "Please enter a valid numeric score.")

def update_labels(score_label, avg_score_label, results):
    last_session_score = results[-1]
    avg_score = sum(results) / len(results)
    
    # Set last session score color
    if 1 <= last_session_score <= 4:
        score_label.config(text=f"Last Session Score: {last_session_score}", fg="red")
    elif 5 <= last_session_score <= 6:
        score_label.config(text=f"Last Session Score: {last_session_score}", fg="yellow")
    elif 7 <= last_session_score <= 8:
        score_label.config(text=f"Last Session Score: {last_session_score}", fg="light green")
    elif 9 <= last_session_score <= 10:
        score_label.config(text=f"Last Session Score: {last_session_score}", fg="green")
    
    # Set average score color
    if 1 <= avg_score <= 4:
        avg_score_label.config(text=f"Average Score: {avg_score:.2f}", fg="red")
    elif 5 <= avg_score <= 6:
        avg_score_label.config(text=f"Average Score: {avg_score:.2f}", fg="yellow")
    elif 7 <= avg_score <= 8:
        avg_score_label.config(text=f"Average Score: {avg_score:.2f}", fg="light green")
    elif 9 <= avg_score <= 10:
        avg_score_label.config(text=f"Average Score: {avg_score:.2f}", fg="green")

def main():
    global total_elapsed_time
    root = tk.Tk()
    root.title("Productivity Timer")
    root.geometry("300x350")

    title_label = tk.Label(root, text="Productivity Timer", font=("Helvetica", 16))
    title_label.pack(pady=10)

    instructions_label = tk.Label(root, text="Set timer (in minutes) and rate your productivity after the session:")
    instructions_label.pack(pady=5)

    timer_label = tk.Label(root, text="Time Remaining: --:--")
    timer_label.pack()

    score_label = tk.Label(root, text="Last Session Score: N/A")
    score_label.pack()

    avg_score_label = tk.Label(root, text="Average Score: N/A")
    avg_score_label.pack()

    total_elapsed_label = tk.Label(root, text="Total Elapsed Time: 0 minutes 0 seconds")
    total_elapsed_label.pack()

    minutes_entry = tk.Entry(root)
    minutes_entry.pack(pady=5)

    score_entry = tk.Entry(root)
    submit_button = tk.Button(root, text="Submit", command=lambda: submit_score(score_entry, root, results, score_label, avg_score_label, submit_button))

    def start_button_click():
        minutes_str = minutes_entry.get()
        try:
            minutes = float(minutes_str)
            if minutes <= 0:
                messagebox.showerror("Error", "Please enter a positive value for minutes.")
            else:
                start_timer(minutes, timer_label, score_label, avg_score_label, total_elapsed_label, results, score_entry, submit_button)
        except ValueError:
            messagebox.showerror("Error", "Please enter a valid numeric value for minutes.")

    def start_timer_with_shortcut(event):
        start_button_click()

    root.bind('<Control-Return>', start_timer_with_shortcut)  # Bind Ctrl + Enter to start the timer

    start_button = tk.Button(root, text="Start Timer", command=start_button_click)
    start_button.pack(pady=10)

    results = []

    root.mainloop()

if __name__ == "__main__":
    main()
